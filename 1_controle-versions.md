# Contrôle de versions

![](https://swcarpentry.github.io/git-novice/fig/phd101212s.png)

Objectifs de la séance :

	- Installer et configurer git et le connecter avec un compte Forgemia associé au group UMR-ASTRE

	- Mettre en place un repo local de test et faire un suivi de versions

	- Intégrer des contributions de tiers et contribuer aux repos d'autres


## Intro

https://swcarpentry.github.io/git-novice/01-basics.html


## Motivation

- Rôle central dans le developpement logiciel. Ressemblance avec des activités scientifiques. 

- Opérationalisation des idéaux de [Science Ouverte et Réproducibilité](https://swcarpentry.github.io/git-novice/10-open/index.html).

- Comme pour "Track changes" mais "on steroids". Travail progressif, cumulatif. Meme "solo", mais surtout pour coordoner les contributions de façon asynchrone. 

- Garder trace des modifications. Capacité de récupérer des choses abandonées, ou d'avoir plusieurs lignes de travail parallèles.

- Travailler "off-line". Intégrer (presque) automatiquement.

- Formaliser la démarche "naturelle" de prendre le travail d'une autre personne et faire des modifications sur une copie.

![](https://github.com/jennybc/excuse-me-iris/raw/master/diy-vs-git-solo-vs-collab.png)

- Matérialiser toute la chaîne de traitement __dès les données aux produits de la recherche__.

- Applicable au texte, code, données (mais pas à des formats binaires).



## Exemples

Présenter l'interface GitLab, le graphe de commits, les contenus d'un commit

- Le dépôt de ces mêmes matériaux :  https://forgemia.inra.fr/umr-astre/training/smcr

- Une analyse en collaboration : https://forgemia.inra.fr/umr-astre/asf-challenge

- [Dévelopment logiciel](https://forgemia.inra.fr/umr-astre/mapMCDA), [documentation](https://forgemia.inra.fr/umr-astre/guidelines/doc-references), [articles scientifiques](https://forgemia.inra.fr/umr-astre/manuscripts/seromonitoring-prob-protection), [slide presentations](https://forgemia.inra.fr/umr-astre/presentations/reproducible-research-in-r), [teaching materials](https://forgemia.inra.fr/umr-astre/training/notions_stats), [data-analysis reports](https://forgemia.inra.fr/umr-astre/capiche), livres, tableaux de bord, visualisations, etc.

## Démarrer en pratique


https://forgemia.inra.fr/umr-astre/training/forgemia-tutorial


## Concepts à retenir

- local/remote repository, commit, branch, merge, tag, hosting service (GitHub, GitLab)


## Exercise final

Cloner ce dépôt (par SSH ou par HTTPS) : 

`git clone git@forgemia.inra.fr:umr-astre/training/smcr.git`

ou bien

`git clone https://forgemia.inra.fr/umr-astre/training/smcr`

Ajouter un fichier de texte nommé `participants.md` avec une ligne contenant votre nom. Commiter et pusher.



## Références

https://www.scicompr.com/slides/week00/00a-git-github-install.html#1

https://www.scicompr.com/slides/week01/01a-intro-toolkit.html#50

https://aosmith16.github.io/spring-r-topics/slides/week01_introduction_git_and_github.html#1


[1] https://happygitwithr.com/

[2] https://swcarpentry.github.io/git-novice/

[2] https://www.book.utilitr.org/

[3] https://peerj.com/preprints/3159v2/#

[4] https://ohshitgit.com/
