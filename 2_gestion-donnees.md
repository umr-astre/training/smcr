# Gestion de données

Objectifs de la séance :

- Identifier les différents __besoins__ des activités de stockage, analyse et visualisation de données.

- Mettre en oeuvre des __bonnes pratiques__ de nommage de fichiers et variables sur un jeux de données propre.

- Organiser ses données selon les principes _tidy_

- Entamer un _data dictionary_ pour ses données

- Créer un __Dataverse__ pour son projet et deposer ses données

- Associer les __collaborateurs__ au Dataverse


1. Principes de sécurité et réproductibilité; Principes d'organisation; Documentation de données; Travail Pratique.

	[diapos FR](https://umr-astre.pages.mia.inra.fr/training/gestion-donnees/) · [slides EN](https://umr-astre.pages.mia.inra.fr/training/data-management/)

	https://umr-astre.pages.mia.inra.fr/guidelines/statstips/

	- Travail Pratique

		__Votre mission__ : appliquer ces principes d'organisation à un des vos projets. 

		- Re-organisez vos fichiers dans une structure de répertoires convenable

		- Renommez les fichiers pour les rendre descriptifs et normalisés

		- Restructurez un jeu de données qui le mérite

		- Écrivez la documentation d'un jeu de données

		__Documentez votre démarche__. 
		Prenez quelques captures d'écran avant et après. Expliquez ce que vous avez changé et pour qoi.
		Ceci sera votre __livrable__ pour cette séquence.

		Notes: Travaillez sur une __copie__ du projet pour vous permettre expérimenter. Ne soyez pas exhaustif. C'est juste un exercise. Quelques exemples de chaque chose suffisent.


2. Un package R pour guider la documentation en suivant des standards établis : https://docs.ropensci.org/dataspice/index.html


3. Données ouverts 

	- Au Cirad : https://coop-ist.cirad.fr/gerer-des-donnees/diffuser-les-donnees/pourquoi-ouvrir-les-donnees-de-la-recherche

	- Contexte National : 
		- [Guide de bonnes pratiques sur la gestion des données de la recherche](https://mi-gt-donnees.pages.math.unistra.fr/guide/00-introduction.html)

		- [Plan National pour la Science Ouverte](https://www.ouvrirlascience.fr/plan-national-pour-la-science-ouverte/)

	- Contexte Européen : [Recommandation (UE) 2018/790 de la Commission du 25 avril 2018 relative à l'accès aux informations scientifiques et à leur conservation](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=uriserv%3AOJ.L_.2018.134.01.0012.01.FRA&toc=OJ%3AL%3A2018%3A134%3ATOC)

		> Les États membres devraient ... veiller : 
		> 
		> 	- à que toutes les publications scientifiques issues de la recherche financée par des fonds publics soient mises à disposition en libre accès à compter de 2020 au plus tard, 
		> 	- ... 
		>	- à ce que la planification de la gestion des données devienne une pratique scientifique courante dès le début du processus de recherche lorsque des données sont générées ou collectées, notamment par l'obligation de prévoir des __plans pour la de gestion des données__,
		> 	- à ce que les données de la recherche financée par des fonds publics deviennent et demeurent faciles à trouver, accessibles, interopérables et réutilisables (principes FAIR) ... 

3. Dataverse https://dataverse.cirad.fr

	- Guide Dataverse https://revelecist.cirad.fr/dataverse/guide

	- Concepts : 
		- Dataverse ⊇ Dataverses (_collections_), Datasets ⊇ Fichers + Méta-données
		- Permissions ⊆ Roles ⊇ Groups ⊇ Users

	- Création de compte :

		- Personnel ASTRE

			1. se connecter avec ses crédentielles Cirad une fois pour créer le compte. Vous ne voyez que les données publiés.

			2. Gestionnaire : les chercher et ajouter au groupe ASTRE-Collaborateurs du Dataverse UMR-ASTRE.

		- [Partenaires extérieurs](https://forgemia.inra.fr/umr-astre/guidelines/doc-references/-/wikis/Comment-donner-acc%C3%A9s-%C3%A0-Dataverse-ou-Alfresco-%C3%A0-personnes-ext%C3%A9rieures-au-Cirad)


	- Gestionnaire : ajouter participants en tant que membres ou collaborateurs du sous-dataverse de smcr-test. Observer les differents droits de chaque un.

	- Les privilèges ne se transmettent pas aux sous-dataverse. Il faut toujours définir les permisions.



4. Un package R pour intéragir avec Dataverse : https://iqss.github.io/dataverse-client-r/

