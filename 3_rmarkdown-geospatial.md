# Reproducibilité et données géographiques en R

Objectifs de la séance

- Faire évoluer un script R en rapport `RMarkdown`

- Structurer un analyse en forme de pipeline `targets`

- Importer des données géo-spatiaux

- Visualiser des données géo-spatiaux et faire des cartes


[Presentation slides](https://umr-astre.pages.mia.inra.fr/presentations/reproducible-research-in-r/#/demo---case-study)

[Recording](https://av.tib.eu/media/13751)


## RMarkdown Intro

Travailler avec R est très pratique, car il suffit d'enregistrer le __script__ qui produit tous les résultats. 
Un script R est simplement un fichier texte (avec l'extension conventionnelle `.R`) contenant la séquence de commandes et d'appels de fonctions permettant d'effectuer l'analyse souhaitée.

Si vous voulez __vérifier d'où vient un résultat__, il vous suffit de regarder dans le script, qui est dans ce sens _auto-documenté_.
Il est donc (presque) trivial de __reproduire__ les résultats, que ce soit par vous-même ou par quelqu'un d'autre. 
En toute justice, les problèmes surviennent souvent en raison des différences dans l'environnement sous-jacent (versions des paquets, plate-forme, etc.).
Toutefois, c'est un bon début.

Une approche encore meilleure consiste à __coder entièrement le rapport__ d'analyse. 
Réaliser l'analyse (par exemple avec un script R) puis transférer les résultats (par exemple par copier-coller) dans un manuscrit en plusieurs étapes distinctes est source d'erreurs et difficile à synchroniser car (inévitablement) les calculs sont mis à jour et corrigés continuellement pendant tout la durée du projet y compris la rédaction des articles et même après.
Au lieu de cela, nous pouvons __écrire le manuscrit avec le code nécessaire pour produire les résultats__, le tout dans un seul document, qui est ensuite __compilé__ dans un rapport.

Cette approche, appelée [Literate Programming](https://en.wikipedia.org/wiki/Literate_programming) (_document computationnel_), n'est pas vraiment nouvelle : elle a été introduite par Donald Knuth en 1984. 
Dans R, une implémentation populaire de cette idée est [R-Markdown](https://rmarkdown.rstudio.com/).

RStudio intègre R Markdown dans son interface, ce qui rend la prise en main très facile. 
Allez dans `Fichier > Nouveau fichier > R Markdown...` et suivez les instructions.
Sinon, vous pouvez écrire un fichier R-Markdown (qui est un fichier texte avec une extension `.Rmd`) et _compiler_ le rapport en utilisant `rmarkdown::render()`.

Une dernière recommandation pratique. Gardez tous les fichiers (données, code, documentation, rapports) d'un projet de organisés dans un répertoire.
Si vous utilisez RStudio, la mise en place d'un projet RStudio dans ce répertoire facilite le passage rapide d'un projet à l'autre et le partage de projets entre ordinateurs et personnes.


## Débuter en pratique

1. Exemple démarche : Créer et compiler un document RMarkdown depuis RStudio en 2 clicks.

1. Markdown Quick Reference en RStudio.

1. R Markdown introduction by RStudio. https://rmarkdown.rstudio.com/lesson-1.html

1. Continue to lesson 2: how it works.

1. Aperçu des éléments basiques : https://rmd4sci.njtierney.com/using-rmarkdown.html

1. Exemples : [rapport technique](https://forgemia.inra.fr/umr-astre/confint_ratio), [slides](https://forgemia.inra.fr/umr-astre/presentations/mapMCDA_AQCR), [website](https://umr-astre.pages.mia.inra.fr/training/notions_stats/), [livre](https://geocompr.robinlovelace.net/), [interactive dashboard](https://jjallaire.shinyapps.io/shiny-biclust/)

1. Articles scientifiques ([package `rticles`](https://pkgs.rstudio.com/rticles/))

__Note__ : Plus récemment, [Quarto](https://quarto.org/) se positionne comme le successeur de RMarkdown, en levant certaines limitations, améliorant et ajoutant des fonctionnalités et en le rendant indépendant de R.


__TP__ : Prendre un script à vous et le ré-structurer en RMarkdown. Même si ce n'est que pour l'organiser en sections.



## Pipelines avec le [package `targets`](https://docs.ropensci.org/targets/)

Motivation : Gérér automatiquement le __stockage de résultats intermédiaires__ pour gagner du temps, tout en préservant la __reproductibilité__. Définir un arbre de dépendences pour ré-calculer seulement les éléments strictement nécessaires suite à un changement.

1. Comment ça marche en pratique : [The evolution of the R script](https://forgemia.inra.fr/umr-astre/training/evolution-rscript)

1. Exemple : une pipeline bien remplie (asf-challenge).

1. Exemple : Un [canevas de projet](https://forgemia.inra.fr/famuvie/project-template) pour inspiration.

__TP__ : Externaliser certaines opérations dans le rapport RMarkdown en tant que _targets_


## Données géospatiaux


- Donées géo-spatiaux : Olivier Gimenez 2020. [GIS and mapping in R: Introduction to the sf package](https://oliviergimenez.github.io/intro_spatialR/)

- Robin Lovelace (2022) _Geocomputation with R_ https://geocompr.robinlovelace.net/


## Références

- Hadley Wickham and Garrett Grolemund (2017) R for Data Science.
	- Chapter 8: Workflow: projects. https://r4ds.had.co.nz/workflow-projects.html.
	- Chapter 27: R Markdown. https://r4ds.had.co.nz/r-markdown.html

- Nicholas Tierney (2020) RMarkdown for Scientists
	- Chapter 5: Workflow https://rmd4sci.njtierney.com/workflow.html
	- Chapter 6: Using RMarkdown https://rmd4sci.njtierney.com/using-rmarkdown.html


- Rafael A. Irizarry (2021)Introduction to Data Science. Chapter 40: Reproducible projects with RStudio and R markdown. https://rafalab.github.io/dsbook/reproducible-projects-with-rstudio-and-r-markdown.html

- Will Landau (2022) The targets R Package User Manual https://books.ropensci.org/targets/

- Robin Lovelace (2022) _Geocomputation with R_ https://geocompr.robinlovelace.net/
