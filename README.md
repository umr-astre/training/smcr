# Méthodes collaboratives et reproductibles dans la recherche

Atélier informel destiné stagiaires, doctorants et chercheurs intérésés. Animateur : Facundo Muñoz.

Suite à l'atélier, les participants auront les éléments basiques pour collaborer de façon asynchrone et à distance avec des collègues avec une gestion efficace des contributions et des changements, de produire des rapports automatisés et reproductibles, de gérér et partager efficacement ses données, d'organiser le code R sous forme de pipeline et de traiter des données géographiques en R.

LICENSE d'utilisation : [CC-BY-SA](http://creativecommons.org/licenses/by-sa/4.0/).


## Motivation

- L'enjeux de la _reproducibilité_ dans le contexte de la _Science Ouverte_. [^1] [^2]



## Objectifs opérationnels

1. Gérer des versions successives de son code ou manuscrit avec `git`

2. Déposer et partager des dépôts git sur `Forgemia` (la plateforme GitLab du Département MIA de l'Inrae) et gérer ses collaborations avec des tiers

3. Gérer les jeux de données efficacement (versions, duplication, documentation, nomenclature)

4. Organiser les tableaux de données 

5. Déposer un jeux de données sur le `Dataverse` ASTRE

6. Produire des analyses reproductibles avec `RMarkdown`

7. Structurer un analyse R sous forme de _pipeline_ avec `targets`

8. Importer et visualiser des données géographiques avec `R`


## Calendrier

Les matins du 12 au 14 janvier 2022, de 10h à 12h30 UTC+1. 
En __distanciel__, par [visio-conférence Teams](https://teams.microsoft.com/l/meetup-join/19%3aQNk8LjrC-JwCLEyLXhiXg8mk9BjLBh6rvgAON7H5vks1%40thread.tacv2/1641920924220?context=%7b%22Tid%22%3a%2260af5f90-e3b4-47d6-8c8e-e6d6d2b117b9%22%2c%22Oid%22%3a%22dd6899b6-c525-4c71-87a9-6c837d3cc1ec%22%7d).

- [Mercredi 12 : Contrôle de versions (obj 1-2)](1_controle-versions.md). [__Enregistrement__](https://collaboratif.cirad.fr/share/proxy/alfresco-noauth/api/internal/shared/node/4H1shWQKQUSIr4IBDkSlRw/content/formation_controle_versions.mkv?c=force&noCache=1642521045115&a=true)

- [Jeudi 13 : Gestion de données (obj 3-5)](2_gestion-donnees.md)
	- __Enregistrements__ ([partie 1](https://ciradfr.sharepoint.com/:v:/r/sites/UMRASTRE/Documents%20partages/General/Recordings/Seminaire%20M%C3%A9thodes%20collaboratives%20et%20reproductibles%20dans%20la%20recherche-20220113_100327-Meeting%20Recording.mp4?csf=1&web=1&e=xHVKJc), [partie 2](https://ciradfr.sharepoint.com/:v:/r/sites/UMRASTRE/Documents%20partages/General/Recordings/Seminaire%20M%C3%A9thodes%20collaboratives%20et%20reproductibles%20dans%20la%20recherche-20220113_113454-Meeting%20Recording.mp4?csf=1&web=1&e=udNYDQ))

- [Vendredi 14 : Reproducibilité et données géographiques en R (obj 6-8)](3_rmarkdown-geospatial.md)
	- __Enregistrements__ ([partie 1](https://ciradfr.sharepoint.com/:v:/r/sites/UMRASTRE/Documents%20partages/General/Recordings/Seminaire%20M%C3%A9thodes%20collaboratives%20et%20reproductibles%20dans%20la%20recherche-20220114_100247-Meeting%20Recording.mp4?csf=1&web=1&e=mPNenK), [partie 2](https://ciradfr.sharepoint.com/:v:/r/sites/UMRASTRE/Documents%20partages/General/Recordings/Seminaire%20M%C3%A9thodes%20collaboratives%20et%20reproductibles%20dans%20la%20recherche-20220114_111606-Meeting%20Recording.mp4?csf=1&web=1&e=9V1xOP))
	- [English recording from a workshop](https://av.tib.eu/media/13751)


## Préparation

- Un ordinateur et un compte avec privilèges pour l'installation de logiciels, webcam et micro, bonne connexion à Internet.

- Module 1 : Optionnel, pour gagner du temps : [installer git](https://happygitwithr.com/install-git.html)

- Module 2 : Un jeu de données propre, pour nettoyer, documenter et deposer sur Dataverse.

- Module 3 : Un script de travail pour mettre en forme et à partir duquel faire un rapport.



## Sujets traités

- Git et GitLab [0, 1, 2]

- Data management and Dataverse [4]

- RMarkdown [2, 3]

- R workflow with the package `targets` [5]

- Spatial data in R [6, 7, 8]


## Références

[0] https://forgemia.inra.fr/umr-astre/training/forgemia-tutorial

[1] https://happygitwithr.com/

[2] https://www.book.utilitr.org/

[3] https://rmd4sci.njtierney.com/

[4] https://umr-astre.pages.mia.inra.fr/training/notions_stats/S02_donnees.html

[5] https://docs.ropensci.org/targets/

[6] https://moderndive.netlify.app/index.html

[7] https://mdsr-book.github.io/mdsr2e/

[8] https://geocompr.robinlovelace.net/


---
[^1]: [Video introduction à la reproductibilité](https://d381hmu4snvm3e.cloudfront.net/videos/1y3P4zphWmWA/LD.mp4) (6'49). Extrait du [MOOC Recherche reproductible](https://learninglab.inria.fr/mooc-recherche-reproductible-principes-methodologiques-pour-une-science-transparente/) [Inria Learning Lab](https://learninglab.inria.fr/).

[^2]: [Continuation](https://d381hmu4snvm3e.cloudfront.net/videos/mw6YDf1IeLQ8/LD.mp4) (11'42). Extrait du [MOOC Recherche reproductible](https://learninglab.inria.fr/mooc-recherche-reproductible-principes-methodologiques-pour-une-science-transparente/) [Inria Learning Lab](https://learninglab.inria.fr/).
